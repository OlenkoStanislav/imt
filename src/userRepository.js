export class UserRepository {
    

    constructor() {
        this.BASE_URL = 'https://jsonplaceholder.typicode.com/';
     }

    getData(url) {
        return fetch(url).then(r => r.json());
    }

    getUserById(id) {
        if (!isNaN(+id))
            return this.getData(this.BASE_URL.concat('users/').concat(id));

        return "";
    }

    getAllUsers() {
        return this.getData(this.BASE_URL.concat('users'));
    }
}
